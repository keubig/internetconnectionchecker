"""
Python script to check if my home assistant or freebox is still connected to the internet
"""
import os
import sys
import time

from check_if_online import isOnlineHttpGet
from sms_free import sendSMSFree
from utils_email import sendEmail

# Set the date format in french
# ALSO CHANGE YOUR TIMEZONE IN YOUR OS!! : sudo apt-get install language-pack-fr
# locale.setlocale(locale.LC_ALL, "fr_FR.utf8")
# To get a nice date in french, use :
# time.strftime('%A %d %B %Y à %H:%M:%S')

###########################################


def generateDisconnectedMessage() -> str:
    """
    Return a string according to the actual time and date in a french format

    :return: Return a string according to the actual time and date in a french format
    :rtype: str
    """
    return f"/!\\ Maison déconnectée le {time.strftime('%A %d %B %Y à %H:%M:%S')} /!\\"


# The ALERT_MSG IS SENT TO THEM
EMAIL_LIST_STR = os.getenv("EMAIL_LIST")
EMAIL_SUBJECT = "Alarme Maison"

ADDRESS_TO_CHECK_CONNECTIVITY = os.getenv("ADDRESS_TO_CHECK_CONNECTIVITY")

EMAIL_SENDER = os.getenv("EMAIL_SENDER")
EMAIL_PASSWORD = os.getenv("EMAIL_PASSWORD")

NUMBER_OF_CHECK_STR = os.getenv("NUMBER_OF_CHECK")
TIME_BETWEEN_EACH_CHECK_STR = os.getenv("TIME_BETWEEN_EACH_CHECK")

USER_FREE = os.getenv("USER_FREE")
API_TOKEN_FREE = os.getenv("API_TOKEN_FREE")

if (
    not EMAIL_LIST_STR
    or not ADDRESS_TO_CHECK_CONNECTIVITY
    or not EMAIL_SENDER
    or not EMAIL_PASSWORD
    or not NUMBER_OF_CHECK_STR
    or not TIME_BETWEEN_EACH_CHECK_STR
    or not USER_FREE
    or not API_TOKEN_FREE
):
    print("Error in env variables")
    sys.exit()


EMAIL_LIST = EMAIL_LIST_STR.split(",")
NUMBER_OF_CHECK = int(NUMBER_OF_CHECK_STR)
TIME_BETWEEN_EACH_CHECK = int(TIME_BETWEEN_EACH_CHECK_STR)
###########################################


def sendAlertMessage(msg: str) -> None:
    """
    Send an alert message to my family using the Free API and Gmail

    :param msg: [description]
    :type msg: str
    """
    sendSMSFree(USER_FREE, API_TOKEN_FREE, msg)
    sendEmail(EMAIL_SENDER, EMAIL_PASSWORD, EMAIL_LIST, EMAIL_SUBJECT, msg)


###########################################

for i in range(NUMBER_OF_CHECK):
    IS_ONLINE = isOnlineHttpGet(ADDRESS_TO_CHECK_CONNECTIVITY, timeout=2)
    if not IS_ONLINE:
        message = generateDisconnectedMessage()
        print(message)
        sendAlertMessage(message)
        break
    print("Target is connected")
    time.sleep(TIME_BETWEEN_EACH_CHECK)
