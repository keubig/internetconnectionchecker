"""
Util functions to send SMS
"""
import requests


def sendSMSFree(user: str, apiToken: str, msg: str) -> None:
    """
    Util method to send SMS using Free API

    :param user: User id you can find in your Free account
    :type user: str
    :param apiToken: API Token you can find in your Free account
    :type apiToken: str
    :param msg: SMS Content
    :type msg: str
    """
    response = requests.get(
        f"https://smsapi.free-mobile.fr/sendmsg?user={user}&pass={apiToken}&msg={msg}"
    )
    if response.status_code != 200:
        print(
            f"Error while sending SMS using free, response code : {response.status_code}"
        )
