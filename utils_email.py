"""
Util functions to send an email
"""
import smtplib
import ssl
import time
from typing import List


def sendEmail(
    fromEmail: str, fromPassword: str, toEmailList: List[str], subject: str, body: str
) -> None:
    """
    Send an email to each email in the emailList from

    :param fromEmail: Sender Gmail email
    :type fromEmail: str
    :param fromPassword: Sender Gmail password
    :type fromPassword: str
    :param toEmailList: List of emails to send the email to
    :type toEmailList: List[str]
    :param subject: Subject of the email
    :type subject: str
    :param body: Body of the email
    :type body: str
    """
    port = 465  # For SSL
    smtpServer = "smtp.gmail.com"

    stringToSend = f"Subject: {subject}\n\n{body}"

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtpServer, port, context=context) as server:
        server.login(fromEmail, fromPassword)
        for toEmail in toEmailList:
            server.sendmail(fromEmail, toEmail, stringToSend.encode("utf8"))
            time.sleep(1)
