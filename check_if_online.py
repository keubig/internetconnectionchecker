"""
Utils function to check if a machine / website is connected to the Internet
"""
import os
import socket

import requests


def isOnlinePing(ipAddress: str, count: int = 5) -> bool:
    """
    Check if a machine is online using ping requests.
    Work on linux only because of the condition "1 received"

    :param ipAddress: IP address or dns name of remote host we want to check if online
    :type ipAddress: str
    :param count: Number of ping to do, defaults to 5
    :type count: int, optional
    :return: True if target is online, False otherwise
    :rtype: bool
    """
    for _ in range(count):
        pingResponse = os.popen(f"ping {ipAddress} -c 1").read()
        if "1 received" in pingResponse:  # Ping is ok
            return True
    return False


def isOnlineHttpGet(
    ipAddress: str, count: int = 5, protocol: str = "https", timeout: float = 0.3
) -> bool:
    """
    Check if a machine is online using HTTP(S) GET requests.

    :param ipAddress: IP address or dns name of remote host we want to check if online
    :type ipAddress: str
    :param count: Number of request to make, defaults to 5
    :type count: int, optional
    :param protocol: http or https, defaults to "https"
    :type protocol: str, optional
    :param timeout: Timeout in seconds, defaults to 0.3
    :type timeout: float, optional
    :return: True if target is online, False otherwise
    :rtype: bool
    """
    for _ in range(count):
        try:
            response = requests.get(f"{protocol}://{ipAddress}", timeout=timeout)
            if response.status_code == 200:
                return True
        except requests.exceptions.ConnectionError:
            continue
        except requests.exceptions.ReadTimeout:
            continue
    return False


def isOnlineSocket(ipAddress: str, port: int, timeout: float = 2) -> bool:
    """
    Check if a machine is online using sockets.

    :param ipAddress: IP address or dns name of remote host we want to check if online
    :type ipAddress: str
    :param port: Port on which you want to open the socket
    :type port: int
    :param timeout: Timeout of the connection in seconds, defaults to 2
    :type timeout: float, optional
    :return: True if target is online, False otherwise
    :rtype: bool
    """
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mySocket.settimeout(timeout)
    mySocket.connect((ipAddress, port))
    mySocket.close()
    return True
